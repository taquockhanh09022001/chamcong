import { NextFunction, Request,Response } from "express";
import AppDataSource from "ormconfig";
import Position from "src/entities/Position";
async function createPosition(req:Request, res:Response, next:NextFunction) {
   try {
      await AppDataSource.createQueryBuilder().insert().into(Position)
         .values({
            name: "manager",
            code: "5"
         }).execute()
      await AppDataSource.createQueryBuilder().insert().into(Position)
         .values({
            name: "staff",
            code: "0"
         }).execute()
      return res.status(200).json("Thêm mới chức vụ thành công")
   } catch (error) {
      return next(error);
   }
}
export default createPosition;