import { NextFunction, Request, Response } from 'express';
import User from '../../entities/User_Account';
import AppDataSource from '../../../ormconfig';
import { hashSync, compareSync } from 'bcrypt';
import Department from '../../../src/entities/Department';
import UserInformation from '../../../src/entities/User_Information';
import { httpStatus } from '../../../src/config/error';


async function getAll(req: Request, res: Response, next: NextFunction) {
   try {
      const user = await AppDataSource.getRepository(User).find();
      return res.send(user);
   } catch (err) {
      return next(err);
   }
}

async function getOne(req: Request, res: Response) {
   const user = await AppDataSource.getRepository(User).findOneBy({
      id: req.params.id
   });
   res.json(user);
}



async function create(req: Request, res: Response, next: NextFunction) {
   try {
      const user = await AppDataSource.getRepository(User).findOne({
         where: {
            userName: req.body.userName
         }
      });
      if (user) {
         throw new httpStatus('Tài khoản đã tồn tại', 409);
      } else {
         const department = await AppDataSource.getRepository(Department).findOne({
            where: {
               id: req.body.departmentId
            }
         });
         if (!department) {
            throw new httpStatus(`Không tồn tại phòng ban`, 404);
         } else {
            const password = "abcd1234"
            const hashPassword = hashSync(password, 10);
            const data = {
               userName: req.body.userName,
               password: hashPassword,
               role: req.body.role
            };
            const newUser = await AppDataSource.getRepository(User).create(data);
            newUser.department = department;
            const result = await AppDataSource.getRepository(User).save(newUser);
            await AppDataSource.createQueryBuilder().insert().into(UserInformation).values(
               {
                  userId: newUser.id
               }).execute()
            return res.status(201).send({ message: 'Tài khoản được tạo thành công', result });
         }
      }
   } catch (err) {
      return next(err);
   }
}

//
async function updateAccount(req:Request, res:Response, next:NextFunction) {
   try {
      const checkAcc = await AppDataSource.getRepository(User)
         .createQueryBuilder("UserAccount")
         .where("UserAccount.id = :id", { id: req.params.id })
         .getOne()
      if (!checkAcc) {
         next(new httpStatus("Không tồn tại người dùng", 404))
      } else {
         const account = await AppDataSource.getRepository(User).findOne({ where: { userName: req.body.userName } });
         if (account && checkAcc.userName!==req.body.userName) {
            throw new httpStatus("Tài khoản đã tồn tại", 409);
         } else {
            const department = await AppDataSource.getRepository(Department).findOne({where: {id: req.body.departmentId}});
            if (!department) {
               throw new httpStatus(`Không tồn tại phòng ban`, 404);
            } else {
               const data = {
                  userName: req.body.userName,
                  departmentId: req.body.departmentId
               }
               const result = await AppDataSource.createQueryBuilder()
                  .update(User)
                  .set(data).where("id = :id", { id: req.params.id })
                  .execute();
               return res.status(201).json({ message: 'Sửa thông tin người dùng thành công', result });
            }
         }
      }
   } catch (error) {
      next(error);
   }
}

//người dùng sửa thông tin cá nhân
async function updateUser(req: Request, res: Response, next: NextFunction) {
   try {
      const checkInfUser = await AppDataSource.getRepository(UserInformation)
         .createQueryBuilder("UserInformation")
         .where("UserInformation.userId = :id", { id: req.params.userId })
         .getOne()
      if (!checkInfUser) {
         next(new httpStatus("Không tồn tại người dùng", 404))
      } else {
         const data = {
            fullName: req.body.fullName,
            email: req.body.email,
            gender: req.body.gender,
            phoneNumber: req.body.phoneNumber,
            birthDay: req.body.birthDay
         };
         const result = await AppDataSource.createQueryBuilder()
            .update(UserInformation)
            .set(data).where("userId = :id", { id: req.params.userId })
            .execute();
         return res.status(201).json({ message: 'Sửa thông tin người dùng thành công', result });
      }      
         
   } catch (err) {
      next(err);
   }
}

//admin sửa thông tin



async function deleteUser(req: Request, res: Response, next: NextFunction) {
   try {
      const user = await AppDataSource.getRepository(User).findOne({
         where: { id: req.params.userId }
      });
      if (!user) {
         throw new httpStatus('Không tồn tại tài khoản', 404);
      } else {
         await AppDataSource.getRepository(User).delete(req.params.userId);
         return res.status(204).json({ message: 'Xóa tài khoản thành công' });
      }
   } catch (err) {
      return next(err);
   }
}

async function searchUserName(req: Request, res: Response, next: NextFunction) {
   try {
      const user = await AppDataSource.getRepository(User)
         .createQueryBuilder('user')
         .where('user.userName LIKE :userName', { userName: `%${req.body.userName}%` })
         .getMany();
      return res.json(user);
   } catch (err) {
      return next(err);
   }
}

async function changePassword(req: Request, res: Response, next: NextFunction) {
   try {
      const data = {
         password: req.body.password,
         newPassword: req.body.newPassword,
         enterPassword: req.body.enterPassword
      };
      const checkUser = await AppDataSource.getRepository(User).findOne({ where: { id: req.params.userId } });
      if (!checkUser) {
         throw new httpStatus('Tài khoản không tồn tại', 404);
      } else {
         const checkPassword = await compareSync(req.body.password, checkUser.password);
         if (!checkPassword) {
            throw new httpStatus('Sai mật khẩu cũ', 400);
         } else {
            if (data.newPassword !== data.enterPassword) {
               throw new httpStatus('Nhập lại mật khẩu không đúng', 400);
            } else {
               const password = hashSync(data.newPassword, 10);
               const result = await AppDataSource.getRepository(User).update({ id: req.params.userId }, { password });
               return res.status(200).json({ message: 'Đổi mật khẩu thành công', result });
            }
         }
      }
   } catch (err) {
      return next(err);
   }
}

const crudUser = {
   getAll,
   getOne,
   updateAccount,
   updateUser,
   create,
   deleteUser,
   searchUserName,
   changePassword
};
export default crudUser;
