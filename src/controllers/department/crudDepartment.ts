import { Request, Response, NextFunction } from 'express';
import Department from '../../../src/entities/Department';
import AppDataSource from '../../../ormconfig';
import User from '../../entities/User_Account';
import Position from '../../entities/Position';
import { httpStatus } from '../../../src/config/error';


async function getAll(req: Request, res: Response, next: NextFunction) {
   try {
      const department = await AppDataSource.getRepository(Department).find();
      return res.json(department);
   } catch (err) {
      return next(err);
   }
}

async function createDepartment(req: Request, res: Response, next: NextFunction) {
   try {
      const data = {
         name: req.body.name,
      };
      const department = await AppDataSource.getRepository(Department).findOne({ where: { name: req.body.name } });
      if (department) {
         throw new httpStatus("Phòng ban đã tồn tại", 409);
      } else {
         const user = await AppDataSource.getRepository(User).findOne({
            where: { id: req.body.userId }
         });
         //nếu nhập người dùng mà ko có
         if (!user && req.body.userId) {
            throw new httpStatus("Người dùng không tồn tại", 404);
         } else {
            //nếu không nhập người dùng
            const departmentNew = await AppDataSource.getRepository(Department).create(data);
            //departmentNew.manager = user;
            const result = await AppDataSource.getRepository(Department).save(departmentNew);
            return res.status(201).send({ message: `Phòng ban ${data.name} được thêm thành công`, result });
         }
      }
   } catch (err) {
      return next(err);
   }
}

async function updateDepartment(req: Request, res: Response, next: NextFunction) {
   interface IReqBody {
      departmentName: string;
      userId: string;
   }
   const { departmentName, userId } = req.body as IReqBody;
   try {
      const department = await AppDataSource.getRepository(Department).findOne({
         where: { id: req.params.departmentId }
      });
      if (!department) {
         throw new httpStatus("Phòng ban không tồn tại", 404);
      } else {
         const checkDepartment = await AppDataSource.getRepository(Department).findOne({
            where: { name: departmentName }
         });
         if (checkDepartment && department.name !== checkDepartment.name) {
            throw new httpStatus("Phòng ban đã tồn tại", 409);
         } else {
            if (!req.body.userId) {
               const departmentNew = await AppDataSource.createQueryBuilder()
                  .update(Department)
                  .set({ name: departmentName })
                  .where('id = :departmentId', { departmentId: req.params.departmentId })
                  .execute();
               return res.status(200).json({ departmentNew, message: 'Sửa thông tin phòng ban thành công' });
            } else {
               const user = await AppDataSource.getRepository(User)
                  .createQueryBuilder('user')
                  .where('user.departmentId = :departmentId', { departmentId: req.params.departmentId })
                  .andWhere('user.id = :userId', { userId: userId })
                  .getOne();
               if (!user) {
                  throw new httpStatus('Người dùng không thuộc phòng ban', 404);
               } else {
                  const positionStaff = await AppDataSource.getRepository(Position).createQueryBuilder('position')
                     .where('position.code = :code', { code: 0 }).getOne() //lấy ra chức vụ nhân viên bằng mã code

                  const positionManager = await AppDataSource.getRepository(Position).createQueryBuilder('position')
                     .where('position.code = :code', { code: 5 }).getOne() //lấy ra chức vụ quản lý bằng mã code
                  
                  if (!positionManager && !positionStaff){
                     throw new httpStatus("Bạn chưa tạo chức vụ cho phòng ban", 404);
                  } else {
                     await AppDataSource.createQueryBuilder()
                        .update(User)
                        .set({ positionId: positionStaff.id })
                        .where('positionId = :positionId', { positionId: positionManager.id })
                        .execute(); //sửa nhân viên có chức vụ quản lý thành nhân viên

                     await AppDataSource.createQueryBuilder()
                        .update(User)
                        .set({ positionId: positionManager.id })
                        .where('id = :userId', { userId: userId })
                        .execute();

                     const departmentNew = await AppDataSource.createQueryBuilder()
                        .update(Department)
                        .set({ name: departmentName })
                        .where('id = :departmentId', { departmentId: req.params.departmentId })
                        .execute();
                     return res.status(200).json({ departmentNew, message: 'Sửa thông tin phòng ban thành công' });
                  }
               }
            }
         }
      }
   } catch (err) {
      return next(err);
   }
}

async function deleteDepartment(req: Request, res: Response, next: NextFunction) {
   try {
      const department = await AppDataSource.getRepository(Department).findOne({
         where: { id: req.params.departmentId },
      });
      if (!department) {
         throw new httpStatus(`Phòng ban không tồn tại`, 404);
      } else {
         const result = await AppDataSource.getRepository(Department).delete(req.params.departmentId);
         return res.send({ message: `Phòng ban xóa thành công`, result });
      }
   } catch (err) {
      return next(err);
   }
}

async function searchDepartment(req: Request, res: Response) {
   const department = await AppDataSource.getRepository(Department)
      .createQueryBuilder('department')
      .where('department.name LIKE :name', { name: `%${req.body.name}%` })
      .getMany();
   return res.json(department);
}

async function getUsersOfDepartment(req: Request, res: Response) {
   const user = await AppDataSource.getRepository(User).find({ where: { departmentId: req.params.departmentId } });
   return res.send(user);
}

const crudDepartment = {
   getAll,
   createDepartment,
   deleteDepartment,
   updateDepartment,
   searchDepartment,
   getUsersOfDepartment
};
export default crudDepartment;
