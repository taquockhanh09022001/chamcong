import TimeKeeping from "../../../src/entities/TimeKeeping";
import { Request, Response, NextFunction } from "express";
import AppDataSource from "../../../ormconfig";
import { httpStatus } from "../../../src/config/error";

async function timeKeeping(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const datetime = new Date();

    const time =
      datetime.getHours() +
      ":" +
      datetime.getMinutes() +
      ":" +
      datetime.getSeconds();
    const date = new Date(
      datetime.getFullYear(),
      datetime.getMonth(),
      datetime.getDay()
    );
    const checkTimekeeping = await AppDataSource.getRepository(TimeKeeping)
      .createQueryBuilder("timekeeping")
      .where("timekeeping.userId = :id", { id: req.body.userId })
      .andWhere("timekeeping.date = :date", { date: date })
      .getOne();
    if (!checkTimekeeping) {
      const data = {
        userId: req.body.userId,
        date: date,
        arrivalTime: time,
      };
      const timeKeepingMorning = await AppDataSource.createQueryBuilder()
        .insert()
        .into(TimeKeeping)
        .values(data)
        .execute();
      return res.sendStatus(201).send({
        timeKeepingMorning,
        message: "Bạn đã chấm công buổi sáng thành công",
      });
    } else {
      const data = {
        timeBack: time,
      };
      const checkTimekeepingAfternoon = await AppDataSource.getRepository(
        TimeKeeping
      )
        .createQueryBuilder("timekeeping")
        .where("timekeeping.userId = :id", { id: req.body.userId })
        .andWhere("timekeeping.date = :date", { date: date })
        .andWhere("timekeeping.timeBack is not null")
        .getOne();
      if (checkTimekeepingAfternoon) {
        throw new httpStatus(
          "Bạn đã chấm xong công ngày hôm nay nên không thể thực hiện thêm",
          409
        );
      } else {
        const timeKeepingAfternoon = await AppDataSource.createQueryBuilder()
          .update(TimeKeeping)
          .set(data)
          .where("id = :id", { id: checkTimekeeping.id })
          .execute();
        return res
          .sendStatus(201)
          .send({ message: "Bạn đã chấm công buổi chiều thành công" });
      }
    }
  } catch (error) {
    return next(error);
  }
}

const timeKeepingTest = {
  timeKeeping,
};
export default timeKeepingTest;
