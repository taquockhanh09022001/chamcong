
import { Column, Entity, OneToMany } from 'typeorm';
import Base from './Base';
import User from './User_Account';

@Entity('department')
export default class Department extends Base {
   @Column({ unique: true })
      name: string;

   @OneToMany(() => User, (user) => user.department)
      users: User[];
}
