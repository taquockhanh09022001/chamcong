import { Entity, Column, ManyToOne, OneToOne, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';
import Base from './Base';
import Department from './Department';
import Position from './Position';
import UserInformation from './User_Information';
import TimeKeeping from './TimeKeeping';


@Entity('userAccount')
export default class UserAccount extends Base {
   @Column({
      type: 'varchar',
      length: 255,
      unique: true
   })
      userName: string;

   @Column({
      type: 'varchar',
      length: 255
   })
      password: string;

   @Column({
      default: false
   })
      role: boolean;

   @OneToOne(() => UserInformation, (userInformation) => userInformation.user)
      userInformation: UserInformation;

   @Column()
      departmentId: string;

   @ManyToOne(() => Department, (department) => department.users, {
      onDelete: "CASCADE",
      onUpdate: "CASCADE"
   })
      department: Department;

   @Column({ 
      default: null
   })
      positionId: string;

   @ManyToOne(() => Position, (position) => position.users, {
      onDelete: "CASCADE",
      onUpdate: "CASCADE"
   })
      position: Position;

   @OneToMany(() => TimeKeeping, (timeKeeping) => timeKeeping.user)
      timeKeepings: TimeKeeping[];

   @CreateDateColumn()
      createAt: string;

   @UpdateDateColumn()
      updateAt: string;
}
