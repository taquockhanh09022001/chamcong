import { Column, Entity, OneToMany } from 'typeorm';
import Base from './Base';
import User from './User_Account';

@Entity('position')
export default class Position extends Base {
  @Column({ type: 'varchar',
     unique:true })
     name: string;

  @Column(
     {unique:true}
  )
     code: string;

  @OneToMany(() => User, (user) => user.position,{
   
  })
     users: User[];
}
