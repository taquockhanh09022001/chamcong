import { Column, Entity, ManyToOne, OneToMany, Timestamp } from "typeorm";
import Base from "./Base";
import User from "./User_Account";

@Entity("Timekeeping")
export default class TimeKeeping extends Base {
  @Column({
     type: "date",
     default: null,
  })
     date: Date;

  @Column({
     type: "time",
     default: null,
  })
     arrivalTime: Date;

  @Column({
     type: "time",
     default: null,
  })
     timeBack: Date;

  @Column({
     default: null,
  })
     userId: string;

  @ManyToOne(() => User, (user) => user.timeKeepings)
     user: User;
}
