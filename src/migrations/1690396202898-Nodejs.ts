import { MigrationInterface, QueryRunner } from "typeorm";

export class Nodejs1690396202898 implements MigrationInterface {
   name = 'Nodejs1690396202898'

   public async up(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.query(`CREATE TABLE \`base\` (\`id\` varchar(36) NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
      await queryRunner.query(`CREATE TABLE \`department\` (\`id\` varchar(36) NOT NULL, \`name\` varchar(255) NOT NULL, UNIQUE INDEX \`IDX_471da4b90e96c1ebe0af221e07\` (\`name\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
      await queryRunner.query(`CREATE TABLE \`userInformation\` (\`userId\` varchar(255) NOT NULL, \`fullName\` varchar(255) NULL, \`email\` varchar(255) NULL, \`gender\` tinyint NULL, \`birthDay\` date NULL, \`phoneNumber\` varchar(255) NULL, UNIQUE INDEX \`REL_362b1cf8b6ab9bc4c0b7035e73\` (\`userId\`), PRIMARY KEY (\`userId\`)) ENGINE=InnoDB`);
      await queryRunner.query(`CREATE TABLE \`Timekeeping\` (\`id\` varchar(36) NOT NULL, \`date\` date NULL, \`arrivalTime\` time NULL, \`timeBack\` time NULL, \`userId\` varchar(255) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
      await queryRunner.query(`CREATE TABLE \`userAccount\` (\`id\` varchar(36) NOT NULL, \`userName\` varchar(255) NOT NULL, \`password\` varchar(255) NOT NULL, \`role\` tinyint NOT NULL DEFAULT 0, \`departmentId\` varchar(255) NOT NULL, \`positionId\` varchar(255) NULL, \`createAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updateAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), UNIQUE INDEX \`IDX_b2317f40329d51663e909728e0\` (\`userName\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
      await queryRunner.query(`CREATE TABLE \`position\` (\`id\` varchar(36) NOT NULL, \`name\` varchar(255) NOT NULL, \`code\` varchar(255) NOT NULL, UNIQUE INDEX \`IDX_94b556b24267b2d75d6d05fcd1\` (\`name\`), UNIQUE INDEX \`IDX_01e858cf7d6aa6b8f738b14997\` (\`code\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
      await queryRunner.query(`ALTER TABLE \`userInformation\` ADD CONSTRAINT \`FK_362b1cf8b6ab9bc4c0b7035e73c\` FOREIGN KEY (\`userId\`) REFERENCES \`userAccount\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
      await queryRunner.query(`ALTER TABLE \`Timekeeping\` ADD CONSTRAINT \`FK_060ef6fa88ae46d6d8c74b02afc\` FOREIGN KEY (\`userId\`) REFERENCES \`userAccount\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
      await queryRunner.query(`ALTER TABLE \`userAccount\` ADD CONSTRAINT \`FK_568f0fc1ea09d4c3e590adcb50c\` FOREIGN KEY (\`departmentId\`) REFERENCES \`department\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`);
      await queryRunner.query(`ALTER TABLE \`userAccount\` ADD CONSTRAINT \`FK_b0ebba44ef2a3e73d66b984d272\` FOREIGN KEY (\`positionId\`) REFERENCES \`position\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`);
   }

   public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.query(`ALTER TABLE \`userAccount\` DROP FOREIGN KEY \`FK_b0ebba44ef2a3e73d66b984d272\``);
      await queryRunner.query(`ALTER TABLE \`userAccount\` DROP FOREIGN KEY \`FK_568f0fc1ea09d4c3e590adcb50c\``);
      await queryRunner.query(`ALTER TABLE \`Timekeeping\` DROP FOREIGN KEY \`FK_060ef6fa88ae46d6d8c74b02afc\``);
      await queryRunner.query(`ALTER TABLE \`userInformation\` DROP FOREIGN KEY \`FK_362b1cf8b6ab9bc4c0b7035e73c\``);
      await queryRunner.query(`DROP INDEX \`IDX_01e858cf7d6aa6b8f738b14997\` ON \`position\``);
      await queryRunner.query(`DROP INDEX \`IDX_94b556b24267b2d75d6d05fcd1\` ON \`position\``);
      await queryRunner.query(`DROP TABLE \`position\``);
      await queryRunner.query(`DROP INDEX \`IDX_b2317f40329d51663e909728e0\` ON \`userAccount\``);
      await queryRunner.query(`DROP TABLE \`userAccount\``);
      await queryRunner.query(`DROP TABLE \`Timekeeping\``);
      await queryRunner.query(`DROP INDEX \`REL_362b1cf8b6ab9bc4c0b7035e73\` ON \`userInformation\``);
      await queryRunner.query(`DROP TABLE \`userInformation\``);
      await queryRunner.query(`DROP INDEX \`IDX_471da4b90e96c1ebe0af221e07\` ON \`department\``);
      await queryRunner.query(`DROP TABLE \`department\``);
      await queryRunner.query(`DROP TABLE \`base\``);
   }

}
