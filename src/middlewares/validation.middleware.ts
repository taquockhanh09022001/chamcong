import { Joi } from "express-validation";

//user
const login = {
   body: Joi.object({
      userName: Joi.string().min(4).required(),
      password: Joi.string().required(),
      //.regex(/^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})/)
      //.required(),
   }),
};

const createUser = {
   body: Joi.object({
      userName: Joi.string().min(4).required(),
      role: Joi.boolean().required(),
      departmentId: Joi.string().required(),
   }),
};

const updateAccount = {
   body: Joi.object({
      userName: Joi.string().min(4).required(),
      departmentId: Joi.string().required()
   })
}

const updateUser = {
   body: Joi.object({
      fullName: Joi.string().min(3).required(),
      gender:Joi.boolean().required().allow(""),
      birthDay: Joi.date().required().allow(""),
      email: Joi.string().email().required().allow(""),
      phoneNumber: Joi.string().regex(/(84|0[3|5|7|8|9])+([0-9]{8})\b/).allow(""),
   }),
};

const changePassword = {
   body: Joi.object({
      password: Joi.string().optional().allow(""),
      newPassword: Joi.string().min(8).required(),
      enterPassword: Joi.string(),
   }),
};

//department
const createDepartment = {
   body: Joi.object({
      name: Joi.string()
         .regex(/[a-zA-Z]/)
         .required(),
      userId: Joi.string().optional().allow(""),
   }),
};

const updateDepartment = {
   body: Joi.object({
      departmentName: Joi.string()
         .regex(/[a-zA-Z]/)
         .required(),
      userId: Joi.string().optional().allow(""),
   }),
};

const validation = {
   login,
   createUser,
   updateAccount,
   updateUser,
   changePassword,
   createDepartment,
   updateDepartment,
};
export default validation;
