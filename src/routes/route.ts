import { validate } from "express-validation";

import { Router } from "express";
import validation from "../middlewares/validation.middleware";
import crudUser from "../controllers/user/crudUser";
import crudDepartment from "../controllers/department/crudDepartment";
import timeKeepingTest from "../controllers/timeKeeping/timeKeeping";
const router = Router();

//timekeeping
router.route("/timeKeeping").post(timeKeepingTest.timeKeeping);

//Department
router.route("/department").get(crudDepartment.getAll);
router
   .route("/department/create")
   .post(validate(validation.createDepartment), crudDepartment.createDepartment);
router
   .route("/department/update/:departmentId")
   .patch(
      validate(validation.updateDepartment),
      crudDepartment.updateDepartment
   );
router
   .route("/department/delete/:departmentId")
   .delete(crudDepartment.deleteDepartment);
router
   .route("/department/:departmentId")
   .get(crudDepartment.getUsersOfDepartment);
//User
router.route("/user").get(crudUser.getAll); //route trả về tất cả user
router
   .route("/user/create")
   .post(validate(validation.createUser), crudUser.create); //route tạo tài khoản

router
   .route("/user/updateUser/:userId")
   .patch(validate(validation.updateUser), crudUser.updateUser); //route sửa thông tin người dùng

router
   .route("/user/updateAccount/:id")
   .patch(validate(validation.updateAccount), crudUser.updateAccount);

router
   .route("/user/ChangePassword/:userId")
   .patch(validate(validation.changePassword), crudUser.changePassword);
router.route("/user/search").get(crudUser.searchUserName); //route tìm kiếm user theo userName
router.route("/user/:userId").delete(crudUser.deleteUser); //route xóa user

export default router;
