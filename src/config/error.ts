export interface httpStatus {
   message: string;
   status: number
}

export class httpStatus extends Error {
   constructor(message: string, status: number) {
      super(message);
      this.status = status;
   }
}
